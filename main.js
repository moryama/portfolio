// TODO: Refactor

// Show and hide portfolio sections

jQuery(function ($) {

  var projectsView = $('#projects-view');
  var pathView = $('#path-view');

  var projectsBtn = $('#show-projects');
  var pathBtn = $('#show-path');

  projectsBtn.click(onClickShowAndHide.bind(this, projectsView, pathView, projectsBtn, pathBtn));
  pathBtn.click(onClickShowAndHide.bind(this, pathView, projectsView, pathBtn, projectsBtn));
})

function onClickShowAndHide(toShow, toHide, toShowBtn, toHideBtn, e) {
  e.preventDefault();
  if (toShow.css('display') == 'none') {
    if (toHide.css('display') == 'block') {
      toHide.hide();
      toHideBtn.css('text-decoration', 'initial');
    }
    toShowBtn.css('text-decoration', 'underline');
    toShow.show();
  } else {
    toShow.hide();
    toShowBtn.css('text-decoration', 'initial');
  }
}

// Add pointers to timeline
var currLi = $('#current');
var beginLi = $('#beginning');

attachPointers();

function attachPointers() {

  currPointer = '<div class="timeline-pointer"><h1>Currently -></h1></div>'
  currLi.append(currPointer);

  beginPointer = '<div class="timeline-pointer"><h1>June 2019 -></h1></div>'
  beginLi.append(beginPointer);
}


// Reverse functionality on learning path

var reverseBtn = $('#reverse-btn')
var currentSign = $('#current')
var beginningSign = $('#beginning')

reverseBtn.click(onClickReversePath);

function onClickReversePath(e) {
  e.preventDefault();
  if (reverseBtn.attr('class') == 'current') {
    reverseBtn.html('Sort from current');
    reverseBtn.attr('class', 'oldest');
    currentSign.attr('id', 'beginning');
    beginningSign.attr('id', 'current');
  } else {
    reverseBtn.html('Sort from oldest');
    reverseBtn.attr('class', 'current');
    currentSign.attr('id', 'current');
    beginningSign.attr('id', 'beginning');
  }
  var pathList = document.getElementById('path-list');
  var i = pathList.childNodes.length;
  while (i--) {
    pathList.appendChild(pathList.childNodes[i]);
  }
}

// Highlight items in list
// Get list
var lis = document.getElementsByTagName('li');

function onChangeHighlight() {
  var select = document.getElementById('highlight-selector');
  var selectedValue = select.value;
  // Loop over the list
  for (var i = 0; i < lis.length; i++) {
    // If the element is selected
    if (lis[i].classList.contains(selectedValue)) {
      // Highlight
      lis[i].classList.add('highlighted');
    } else {
      // Remove highlight if any
      if (lis[i].classList.contains('highlighted')) {
        lis[i].classList.remove('highlighted');
      }
    }
  }
};


// Scroll to top button
//Get the button:
scrollBtn = document.getElementById("scroll-to-top");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {showBtnOnScroll()};

function showBtnOnScroll() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    scrollBtn.style.display = "block";
  } else {
    scrollBtn.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function goToTop() {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}
