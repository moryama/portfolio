## Hi there!

This project is my one page portfolio. See how it [looks like](https://moryama.gitlab.io/portfolio/).

Feel free to use it as a template to build your own portfolio. 

## Credits

**Patterns CSS**

- bright squares: [Waseem Dahman](https://twitter.com/dwaseem)
- tiny squares: [Agencia General](https://www.agenciageneral.com.br/)
- old mathematics: [Josh Green](https://emailcoder.net/)

## License

[Creative Commons BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)